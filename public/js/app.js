class App extends React.Component {
    state = {
        from: '',
        to: '',
        amount: '',
        result: '',
        error: '',
        currencies: [],
        describe: []
    }

    toggleHandler() {
        axios.get('http://localhost:9999', {params: { from: this.state.from, to: this.state.to, amount: this.state.amount}})
            .then((response) => {
                this.setState({result: response.data});
                this.setState({error: ''});

            })
            .catch( (error) => {
                if (error.response.data.error.length > 0) {
                    this.setState({result: ''});
                    this.setState({error: error.response.data.error});
                }
            });
    }

    setFirstCurrency(currency) {
        this.setState({from: currency});
        this.setState({to: currency});
    }

    getDescribe() {
        axios.get('http://localhost:9999', {params: { describe: 1}})
            .then((response) => {
                this.setState({describe: response.data});

                this.setFirstCurrency(Object.keys(this.state.describe)[0]);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    changeHandlersFrom(from) {
        this.setState({
            from
        })
    }

    changeHandlersTo(to) {
        this.setState({
            to
        })
    }

    changeHandlersAmount(amount) {
        this.setState({
            amount
        })
    }

    componentWillMount() {
        this.getDescribe();
    }

    render() {
        return (
            <div className="app">
                <header>
                    { this.state.result }
                </header>
                <div className="error">{ this.state.error }</div>
                <form onSubmit = { e => e.preventDefault() }>
                    <div className="form-group">
                        <label htmlFor='from'>From:</label>
                        <select name='from' onChange={(event) => this.changeHandlersFrom(event.target.value)}>
                            {Object.keys(this.state.describe).map((key) => {
                                return <option value={key} key={key}>{this.state.describe[key].currencyName}</option>;
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor='to'>To:</label>
                        <select name='to' onChange={(event) => this.changeHandlersTo(event.target.value)}>
                            {Object.keys(this.state.describe).map((key) => {
                                return <option value={key} key={key}>{this.state.describe[key].currencyName}</option>;
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor='amount'>Amount:</label>
                        <input type="text" name='amount' value={this.state.amount} onChange={(event) => this.changeHandlersAmount(event.target.value)}/>
                    </div>
                    <button name='convert' onClick={ () => this.toggleHandler() }>Convert</button>
                </form>
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'));