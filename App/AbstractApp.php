<?php

namespace App;

use App\Converter\ConverterInterface;


abstract class AbstractApp
{
    /**
     * Get convert data by custom converter
     *
     * @param ConverterInterface $converter
     * @param $from
     * @param $to
     * @param $amount
     * @return float|int
     */
    public function convert(ConverterInterface $converter, $from, $to, $amount): float
    {
        return $converter->convert($from, $to, $amount) * $amount;
    }

    /**
     * Get convert describe data by custom converter
     *
     * @param ConverterInterface $converter
     * @return array
     */
    public function describe(ConverterInterface $converter): array
    {
        return $converter->getDescribe();
    }

    /**
     * Customise convert data
     *
     * @return float
     */
    abstract public function getConvertResult(): float;

    /**
     * Customise convert describe data
     *
     * @return string
     */
    abstract public function getDescribe(): string;
}