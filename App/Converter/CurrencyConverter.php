<?php

namespace App\Converter;

use App\Providers\CurrProvider;
use App\Providers\ProviderInterface;


class CurrencyConverter implements ConverterInterface
{
    /** @var CurrProvider*/
    private $provider;

    /**
     * FreeCurrAdapter constructor.
     *
     * @param ProviderInterface $provider
     */
    public function __construct(ProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Get converted value
     *
     * @param string $from
     * @param string $to
     * @param int $amount
     * @return mixed
     * @throws \Exception
     */
    public function convert(string $from, string $to, int $amount): float
    {
        $this->provider->convertUrl($from, $to);

        return $this->provider->sentRequestForConvert();
    }

    /**
     * Get describe for available currency
     *
     * @return array
     */
    public function getDescribe(): array
    {
        $describe = require 'describe.php';

        return $describe['results'];

    }
}