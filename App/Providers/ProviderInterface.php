<?php

namespace App\Providers;


interface ProviderInterface
{
    /**
     * Send request
     *
     * @return float
     */
    public function sentRequestForConvert(): float;

    /**
     * Generate url for convert currency
     *
     * @param $from
     * @param $to
     * @return mixed
     */
    public function convertUrl($from, $to);
}