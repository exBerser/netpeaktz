<?php

namespace App\Converter;


interface ConverterInterface
{
    /**
     * Get converted value
     *
     * @param string $from
     * @param string $to
     * @param int $amount
     * @return float
     */
    public function convert(string $from, string $to, int $amount): float;

    /**
     * Get describe for available currency
     *
     * @return array
     */
    public function getDescribe(): array;
}