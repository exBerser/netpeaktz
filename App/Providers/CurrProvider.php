<?php

namespace App\Providers;

use Zend\Http\Client;


class CurrProvider implements ProviderInterface
{
    const URI_CONVERT   = 'http://free.currencyconverterapi.com/api/v5/convert';
    const URI_DESCRIBE  = 'https://free.currencyconverterapi.com/api/v5/currencies';
    const HTTP_METHOD   = 'GET';

    /** @var Client $client Zend http client */
    private $client;
    /** @var string from currency */
    private $from;
    /** @var string to currency */
    private $to;

    /**
     * CurrProvider constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->client->setMethod(self::HTTP_METHOD);
    }

    /**
     * Send request
     *
     * @return float
     * @throws \Exception
     */
    public function sentRequestForConvert(): float
    {
        $response = $this->client->send();

        $result = json_decode($response->getContent(), 1);

        if (isset($result[$this->from . '_' . $this->to])) {
            return $result[$this->from . '_' . $this->to]['val'];
        }

        throw new \Exception('Response doesn\'t have the expected result from currencyconverterapi');
    }

    /**
     * Generate url for convert currency
     *
     * @param $from
     * @param $to
     * @return void
     */
    public function convertUrl($from, $to): void
    {
        $this->from = $from;
        $this->to   = $to;

        $this->client->setUri(self::URI_CONVERT);

        $this->client->setParameterGet([
            'q'         => $this->from . '_' . $this->to,
            'compact'   => 'y',
        ]);
    }
}