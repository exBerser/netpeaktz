<?php

namespace App;

use App\Converter\CurrencyConverter;
use App\Providers\CurrProvider;


class App extends AbstractApp
{
    /**
     * @return float|int
     * @throws \Exception
     */
    public function getConvertResult(): float
    {
        $api = new CurrProvider();
        $converter = new CurrencyConverter($api);

        if (isset($_GET['from']) && isset($_GET['to']) && isset($_GET['amount'])) {
            try {
                $amount = $this->validate($_GET['amount']);
                return $this->convert($converter, $_GET['from'], $_GET['to'], $amount);
            } catch (\InvalidArgumentException $e) {
                echo json_encode(['error' => $e->getMessage()]);
            }
        }

        throw new \Exception('Not enough parameters (Get parameters: from, to, amount)');
    }

    /**
     * @param $value
     * @return int
     * @throws \InvalidArgumentException
     */
    public function validate(string $value): int
    {
        if (is_numeric($value)) {
            return (int) $value;
        }

        $value = trim($value) === '' ? 'empty' : $value;

        throw new \InvalidArgumentException('Input amount only accepts integers. Input was: ' . $value);
    }

    /**
     * @return string
     */
    public function getDescribe(): string
    {
        $api = new CurrProvider();
        $converter = new CurrencyConverter($api);

        return json_encode($this->describe($converter));
    }
}