<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require __DIR__ . '/vendor/autoload.php';

use App\App;

if (isset($_GET['from']) && isset($_GET['to']) && isset($_GET['amount'])) {
    $app = new App();
    echo $app->getConvertResult();
} else if (isset($_GET['describe'])) {
    $app = new  App();
    echo $app->getDescribe();
}else{
    require __DIR__ . '/public/index.html';
}
